package com.android.alston.android319;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ADPActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adp);
    }

    public void clickArrayADPBtn (View view){
        Intent it = new Intent(this,ArrayADPActivity.class);
        startActivity(it);
    }

    public void clickSimADPBtn (View view){
        Intent it = new Intent(this,SimADPActivity.class);
        startActivity(it);
    }

    public void clickCurADPBtn (View view){
        Intent it = new Intent(this,CurADPActivity.class);
        startActivity(it);
    }

    public void clickPlusADPBtn (View view){
        Intent it = new Intent(this,PlusActivity.class);
        startActivity(it);
    }
}
