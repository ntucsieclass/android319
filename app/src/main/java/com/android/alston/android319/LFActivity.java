package com.android.alston.android319;

import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class LFActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    int tmp=0;
    int count=0;
    TextView lfTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics dm = getResources().getDisplayMetrics();
        int hpix = dm.heightPixels;
        int wpix = dm.widthPixels;
        if(hpix > wpix)
            setContentView(R.layout.activity_lf);
        else
            setContentView(R.layout.act_lf2);
        lfTV = (TextView)findViewById(R.id.lf_textview);

        mediaPlayer = MediaPlayer.create(this,R.raw.jazz_mango);
        Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }

    public void clickButton(View view){
        count++;
        lfTV.setText(Integer.toString(count));
        Toast.makeText(this,"clickButton",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        String tmp = lfTV.getText().toString();
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_lf);
        else
            setContentView(R.layout.act_lf2);
        lfTV = (TextView)findViewById(R.id.lf_textview);
        lfTV.setText(tmp);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mediaPlayer.start();
        Toast.makeText(this,"onStart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this,"onResume",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mediaPlayer.seekTo(tmp);
        Toast.makeText(this,"onRestart",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
        tmp = mediaPlayer.getCurrentPosition();
        Toast.makeText(this,"onPause",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this,"onStop",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        Toast.makeText(this,"onDestroy",Toast.LENGTH_SHORT).show();
    }
}
