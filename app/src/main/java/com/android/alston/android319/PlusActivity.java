package com.android.alston.android319;

        import android.Manifest;
        import android.app.ListActivity;
        import android.content.ContentResolver;
        import android.content.pm.PackageManager;
        import android.database.Cursor;
        import android.provider.ContactsContract;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v7.app.AlertDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.SimpleAdapter;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

        import static android.Manifest.permission.*;

public class PlusActivity extends ListActivity {
    public static final int REQUEST_CONTACTS = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_plus);

        int permission = ActivityCompat.checkSelfPermission(
                this,
                READ_CONTACTS);

        if (permission == PackageManager.PERMISSION_GRANTED) {
            getContacts();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_CONTACTS,WRITE_CONTACTS},
                    REQUEST_CONTACTS);
        }
    }

    private void  getContacts(){
        List<Map<String,Object>> contactList = new ArrayList<Map<String,Object>>();
        Map<String,Object> contactMap;

        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        while (cursor.moveToNext())
        {
            contactMap = new HashMap<String,Object>();
            int id = cursor.getInt(
                    cursor.getColumnIndex(
                            ContactsContract.Contacts._ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));
            contactMap.put("nameKey",name);
            Cursor cursorPhone = resolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+id,
                    null,
                    null
            );
            String phone="";
            while (cursorPhone.moveToNext())
            {
                phone = cursorPhone.getString(
                        cursorPhone.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                contactMap.put("phoneKey",phone);
            }

            Toast.makeText(this,id+"/"+name+"/"+phone,Toast.LENGTH_SHORT).show();
            cursorPhone.close();
            contactList.add(contactMap);
        }
        cursor.close();


        SimpleAdapter simplePlusAdapter = new SimpleAdapter(
                this,
                contactList,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{"nameKey","phoneKey"},
                new int[]{android.R.id.text1,android.R.id.text2});
        setListAdapter(simplePlusAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case REQUEST_CONTACTS:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getContacts();
                }else {
                    new AlertDialog.Builder(this)
                            .setMessage("請給我權限～～")
                            .setPositiveButton("OK",null)
                            .show();
                }
                break;
            default:
                break;
        }
    }
}

