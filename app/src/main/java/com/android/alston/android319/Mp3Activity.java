package com.android.alston.android319;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Mp3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mp3);
        MediaPlayer mediaPlayer = MediaPlayer.create(this,R.raw.jazz_mango);
        mediaPlayer.start();
    }
}
