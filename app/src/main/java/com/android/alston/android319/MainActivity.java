package com.android.alston.android319;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void clickHWBtn (View view){
        Intent it = new Intent(this,HelloWorldActivity.class);
        startActivity(it);
        Log.d("@@@@@@","click HW Btn!!!");
    }
    public void clickIOBtn (View view){
        Intent it = new Intent(this,InputActivity.class);
        startActivity(it);
    }
    public void clickPermissionBtn (View view){
        Intent it = new Intent(this,PermissionActivity.class);
        startActivity(it);
    }
    public void clickADPBtn (View view){
        Intent it = new Intent(this,ADPActivity.class);
        startActivity(it);
    }
    public void clickMp3Btn (View view){
        Intent it = new Intent(this,Mp3Activity.class);
        startActivity(it);
    }
    public void clickMusicBtn (View view){
        Intent it = new Intent(this,MusicActivity.class);
        startActivity(it);
    }
    public void clickLFBtn (View view){
        Intent it = new Intent(this,LFActivity.class);
        startActivity(it);
    }
    public void clickTHBtn (View view){
        Intent it = new Intent(this,THActivity.class);
        startActivity(it);
    }
    public void clickSerBtn (View view){
        Intent it = new Intent(this,SerActivity.class);
        startActivity(it);
    }
}
