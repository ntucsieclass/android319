package com.android.alston.android319;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static com.android.alston.android319.InputActivity.EXTRA_NAME;

public class OutputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);
        Intent abc = getIntent();
        String nameInput = abc.getStringExtra(EXTRA_NAME);
        TextView ntv = (TextView)findViewById(R.id.otView);
        ntv.setText(nameInput);
    }
}
