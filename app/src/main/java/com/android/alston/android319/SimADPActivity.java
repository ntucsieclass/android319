package com.android.alston.android319;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SimADPActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_sim_adp);
        SimpleAdapter simpleAdapter = new SimpleAdapter(
                this,
                getData(),
                R.layout.activity_sim_adp,
                new String[]{"imgKey","strKey"},
                new int[]{R.id.imgIcon,R.id.textIcon});
        setListAdapter(simpleAdapter);
    }

    private List<Map<String,Object>> getData(){

        List<Map<String,Object>> iconList = new ArrayList<Map<String,Object>>();
        Map<String,Object> iconMap;

        iconMap = new HashMap<String,Object>();
        iconMap.put("imgKey",R.drawable.google_chrome_icon);
        iconMap.put("strKey","Google");
        iconList.add(iconMap);

        iconMap = new HashMap<String,Object>();
        iconMap.put("imgKey",R.drawable.safari_icon);
        iconMap.put("strKey","Safari");
        iconList.add(iconMap);

        iconMap = new HashMap<String,Object>();
        iconMap.put("imgKey",R.drawable.firefox_icon);
        iconMap.put("strKey","Firefox");
        iconList.add(iconMap);

        return iconList;
    }
}
