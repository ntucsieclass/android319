package com.android.alston.android319;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ArrayADPActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_array_adp);

        ListView listView = new ListView(this);

        ArrayAdapter arrayAdapter= new ArrayAdapter<String>(
                this,
                android.R.layout.simple_expandable_list_item_1,
                getData());

        listView.setAdapter(arrayAdapter);

        setContentView(listView);
    }

    private List<String> getData(){
        List<String> testData = new ArrayList<String>();
        testData.add("測試資料一");
        testData.add("測試資料二");
        testData.add("測試資料三");
        return testData;
    }
}
