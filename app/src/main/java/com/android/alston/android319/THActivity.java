package com.android.alston.android319;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

public class THActivity extends AppCompatActivity {
    TextView timTV;
    public static  Handler handler;
    String time="0.00";
    Thread timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_th);
        timTV = (TextView)findViewById(R.id.timerView);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                timTV.setText(time);
            }
        };
    }

    class ABCThread extends Thread{
        @Override
        public void run() {
            super.run();
            int a=0;
            int b=0;
            int c=0;
            while (true){
                try {
                    Thread.sleep(10);
                    c++;
                    if(c==10) {
                        b++;
                        c=0;
                    }
                    if(b==10) {
                        a++;
                        b=0;
                    }
                    time = Integer.toString(a)+"."+Integer.toString(b)+Integer.toString(c);
                    handler.sendEmptyMessage(0);

                }catch (InterruptedException e){
                    break;
                };
            }
        }
    }
    public void clickStartTimer(View view){
        if(timer == null) {
            timer = new ABCThread();
            timer.start();
        }
    }
    public void clickStopTimer(View view){
        timer.interrupt();
        timer = null;
        timTV.setText("0.00");
    }
}
