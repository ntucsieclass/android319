package com.android.alston.android319;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class SerActivity extends AppCompatActivity {
    private MyService myService = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ser);
    }
    public void clickStartBtn (View view){
        myService = null;
        Intent intent = new Intent(this,MyService.class);
        startService(intent);
    }
    public void clickStopBtn (View view){
        myService = null;
        Intent intent = new Intent(this,MyService.class);
        stopService(intent);
    }
    public void clickBindBtn (View view){
        myService = null;
        Intent intent = new Intent(this,MyService.class);
        bindService(intent,myConnect,BIND_AUTO_CREATE);
    }
    public void clickUnBindBtn (View view){
        myService = null;
        unbindService(myConnect);
    }
    public void clickCallBtn (View view){
        if(myService!=null){
            myService.myLogic();
        }
    }
    private ServiceConnection myConnect = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myService = ((MyService.ABCBinder)service).getService();
            Toast.makeText(SerActivity.this,"onServiceConnected",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(SerActivity.this,"onServiceDisconnected",Toast.LENGTH_SHORT).show();
        }
    };
}
