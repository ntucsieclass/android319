package com.android.alston.android319;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import static android.Manifest.permission.*;

public class PermissionActivity extends AppCompatActivity {
    TextView perTV;
    public static final int REQUEST_CONTACTS = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        int permission = ActivityCompat.checkSelfPermission(
                this,
                READ_CONTACTS);
        perTV = (TextView)findViewById(R.id.permissionTV);

        if (permission == PackageManager.PERMISSION_GRANTED) {
            getContacts();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_CONTACTS,WRITE_CONTACTS},
                    REQUEST_CONTACTS);
        }
    }

    private void  getContacts(){
        ContentResolver resolver = getContentResolver();
        Cursor cursor = resolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        while (cursor.moveToNext())
        {
            int id = cursor.getInt(
                    cursor.getColumnIndex(
                            ContactsContract.Contacts._ID));
            String name = cursor.getString(
                    cursor.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));
            Log.d("#######",id+"/"+name);
        }
        cursor.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode) {
            case REQUEST_CONTACTS:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getContacts();
                }else {
                    new AlertDialog.Builder(this)
                            .setMessage("請給我權限～～")
                            .setPositiveButton("OK",null)
                            .show();
                }
                break;
            default:
                perTV.setText("???");
                break;
        }
    }
}
