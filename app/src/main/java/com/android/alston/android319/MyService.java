package com.android.alston.android319;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Date;

public class MyService extends Service {
    private  ABCBinder abcBinder = new ABCBinder();

    private Handler handler = new Handler();
    private Runnable showTime = new Runnable() {
        @Override
        public void run() {
            String time = new Date().toString();
            Toast.makeText(MyService.this,"Time:"+time,Toast.LENGTH_SHORT).show();
            handler.postDelayed(this,5000);
        }
    };
    public class ABCBinder extends Binder {
        MyService getService(){
            Toast.makeText(MyService.this,"ABCBinder",Toast.LENGTH_SHORT).show();
            return MyService.this;
        }
    }

    public MyService() {
    }
    public void myLogic(){
        handler.post(showTime);
        Toast.makeText(this,"myLogic",Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this,"onCreate",Toast.LENGTH_SHORT).show();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this,"onStartCommand",Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this,"onBind",Toast.LENGTH_SHORT).show();
        return abcBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(this,"onUnbind",Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(showTime);
        Toast.makeText(this,"onDestroy",Toast.LENGTH_SHORT).show();
    }
}
