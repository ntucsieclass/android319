package com.android.alston.android319;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.LinkedList;

public class MusicActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    String musicStatus = "stop";
    Button actBTN;
    LinkedList<Integer> songList;
    int index =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        songList = new LinkedList<Integer>();
        songList.add(R.raw.jazz_mango);
        songList.add(R.raw.fond_memories);
        songList.add(R.raw.take_me_to_the_depths);

        mediaPlayer = MediaPlayer.create(this,songList.get(index));
        actBTN = (Button) findViewById(R.id.actionBTN);
        actBTN.setText("Play");
    }

    public void clickNext(View view){
        if(index==songList.size()-1)
            index = 0;
        else
            index = index+1;
        
        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(this,songList.get(index));
        mediaPlayer.start();
        musicStatus = "start";
        actBTN.setText("Pause");
    }
    public void clickPre(View view){
        if(index==0) {
            index = songList.size()-1;
        }else {
            index = index - 1;
        }
        mediaPlayer.stop();
        mediaPlayer = MediaPlayer.create(this,songList.get(index));
        mediaPlayer.start();
        musicStatus = "start";
        actBTN.setText("Pause");
    }
    public void clickActionBTN(View view){
        if(musicStatus.equals("stop") || musicStatus.equals("pause")) {
            mediaPlayer.start();
            musicStatus = "start";
            actBTN.setText("Pause");
        }else{
            mediaPlayer.pause();
            musicStatus = "pause";
            actBTN.setText("Con");
        }
    }

    public void clickStopBTN(View view){
        mediaPlayer.pause();
        mediaPlayer.seekTo(0);
        musicStatus="stop";
        actBTN.setText("Play");
    }
}
