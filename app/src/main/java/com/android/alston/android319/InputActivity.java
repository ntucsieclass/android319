package com.android.alston.android319;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {
    public static final String EXTRA_NAME ="com.android.alston.android319.InputActivity.EXTRA_NAME";
    EditText nameInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        nameInput = (EditText)findViewById(R.id.inputT);
    }

    public void clickSendBtn (View view){
        String name = nameInput.getText().toString();
        Log.d("@@@@@@@","Name:"+name);
        Intent intent = new Intent(this,OutputActivity.class);
        intent.putExtra(EXTRA_NAME,name);
        startActivity(intent);
    }
}
